

NAME := netbox
VERSION := v2.4.3
ETCDIR := /usr/local/etc/docker
SBINDIR := /usr/local/sbin

NETBOX_IMAGE := ninech/$(NAME):$(VERSION)
NGINX_IMAGE := nginx:1.15-alpine
POSTGRES_IMAGE := postgres:10.4-alpine
REDIS_IMAGE := redis:4-alpine

help:
	@echo "make all"
	@echo "make build"
	@echo "make save"
	@echo "make archive"
	@echo "make upload"
	@echo ""
	@echo "make install"
	@echo ""
	@echo "make create"
	@echo "make start"
	@echo "make stop"

all: build save archive upload clean

clean:
	docker image prune

install:
	[ -d $(ETCDIR)/$(NAME) ] || mkdir -vp -m 0700 $(ETCDIR)/$(NAME)
	install -m 0600 docker-compose.yml $(ETCDIR)/$(NAME)/docker-compose.yml
	install -m 0600 $(NAME).env $(ETCDIR)/$(NAME)/$(NAME).env
	install -m 0600 postgres.env $(ETCDIR)/$(NAME)/postgres.env
	install -m 0600 redis.env $(ETCDIR)/$(NAME)/redis.env
	echo "cd $(ETCDIR)/$(NAME) && docker-compose up --no-start" > $(SBINDIR)/docker-$(NAME)-create
	echo "cd $(ETCDIR)/$(NAME) && docker-compose start" > $(SBINDIR)/docker-$(NAME)-start
	echo "cd $(ETCDIR)/$(NAME) && docker-compose stop" > $(SBINDIR)/docker-$(NAME)-stop
	echo "cd $(ETCDIR)/$(NAME) && docker-compose top" > $(SBINDIR)/docker-$(NAME)-status
	echo "cd $(ETCDIR)/$(NAME) && docker-compose logs" > $(SBINDIR)/docker-$(NAME)-logs
	chmod 0700 $(SBINDIR)/docker-$(NAME)*

build:
	docker-compose pull
	VERSION=$(VERSION) docker-compose build
	
save:
	docker image save $(NETBOX_IMAGE)   | nice xz -z1cv > $(NAME).docker.image.xz
	docker image save $(NGINX_IMAGE)    | nice xz -z1cv > $(NAME)-nginx.docker.image.xz
	docker image save $(POSTGRES_IMAGE) | nice xz -z1cv > $(NAME)-postgres.docker.image.xz
	docker image save $(REDIS_IMAGE)    | nice xz -z1cv > $(NAME)-redis.docker.image.xz
	echo "Run 'make upload' for upload to Google Storage"

load:
	xz -dc $(NAME)-redis.docker.image.xz    | docker image load 
	xz -dc $(NAME)-postgres.docker.image.xz | docker image load
	xz -dc $(NAME)-nginx.docker.image.xz    | docker image load
	xz -dc $(NAME).docker.image.xz          | docker image load

archive:
	tar -c -v -f $(NAME).ARCHIVE.tgz --exclude="*ARCHIVE*" .
	du -hs *ARCHIVE*

clean:
	docker image prune

upload:
	gcloud auth activate-service-account --key-file ~/.gstorage.json
	gsutil -m cp $(NAME)*.docker.image.xz $(NAME).ARCHIVE.tgz gs://net-juenemann-contract9419
	echo "https://console.cloud.google.com/storage/browser/net-juenemann-contract9419?project=playground-168601"

create:
	docker-compose up --no-start

start:
	docker-compose start
	docker-compose logs

stop:
	docker-compose stop

restart: stop start

